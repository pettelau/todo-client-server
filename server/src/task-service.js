import CryptoJS from 'crypto-js';

var mockDB = [
  {
    username: 'Ola',
    password:
      'fdfe4166c9a5a3e77429f33721b26e1e2a66f2bb65e57078e862bc8cff43e69afdc2832fe33d16d07a8780b6f9f31631ea0a48745b8302f17be303f20aa11541',
    validToken: '',
    balance: 1560,
    fullName: 'Ola Jensen',
    address: 'Prinsens gate 14',
    age: 23,
  },
  { username: 'Kari', password: 'Hei123' },
];

let salt = 'Exceptionally good salt';
let hashFinish = false;
let hashedPassServer = 'jj';

class PasswordService {
  /**
   * Get task with given id.
   */
  // get(id: number) {
  //   return new Promise<?Task>((resolve, reject) => {
  //     pool.query('SELECT * FROM Tasks WHERE id = ?', [id], (error, results: Task[]) => {
  //       if (error) return reject(error);

  //       resolve(results[0]);
  //     });
  //   });
  // };

  /**
   * Get all tasks.
   */
  getCredentials(username, token) {
    let balance;
    let fullName;
    let address;
    let age;
    return new Promise((resolve, reject) => {
      for (let i = 0; i < mockDB.length; i++) {
        if (mockDB[i].username == username && mockDB[i].validToken == token) {
          balance = mockDB[i].balance;
          fullName = mockDB[i].fullName;
          address = mockDB[i].address;
          age = mockDB[i].age;
        }
      }
      resolve({ balance: balance, fullName: fullName, address: address, age: age });
    });
  }

  /**
   * Create new task having the given title.
   *
   * Resolves the newly created task id.
   */
  passwordChecker(username, password) {
    let result = { login: false };
    this.hashPass(password);
    if (hashFinish == true) {
      return new Promise((resolve, reject) => {
        //console.log(username, password);
        for (let i = 0; i < mockDB.length; i++) {
          if (mockDB[i].username == username && mockDB[i].password == hashedPassServer.toString()) {
            console.log('true');
            // result = { login: true };
            result = { login: true, token: this.generateToken(i) };
          }
        }
        resolve(result);
        console.log('false');
        return reject(error);
      });
    }

    // return new Promise<number>((resolve, reject) => {
    //   pool.query('INSERT INTO Tasks SET title=?', [title], (error, results) => {
    //     if (error) return reject(error);
    //     if (!results.insertId) return reject(new Error('Id of inserted row not found'));

    //     resolve(Number(results.insertId));
    //   });
    // });
  }
  generateToken(indexOfUser) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < 12; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    mockDB[indexOfUser].validToken = result;
    return result;
  }
  hashPass(password) {
    console.log(password);
    hashedPassServer = CryptoJS.PBKDF2(password, salt, { keySize: 512 / 32 });
    console.log(hashedPassServer.toString());
    hashFinish = true;
  }

  // /**
  //  * Delete task with given id.
  //  */
  // delete(id: number) {
  //   return new Promise<void>((resolve, reject) => {
  //     pool.query('DELETE FROM Tasks WHERE id = ?', [id], (error) => {
  //       if (error) return reject(error);

  //       resolve();
  //     });
  //   });
  // }
}

const passwordService = new PasswordService();
export default passwordService;
