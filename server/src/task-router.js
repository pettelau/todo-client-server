// @flow
import express from 'express';
import taskService from './task-service';
import passwordService from './task-service';

/**
 * Express router containing task methods.
 */
const router = express.Router();

// router.get('/tasks', (request, response) => {
//   taskService
//     .getAll()
//     .then((rows) => response.send(rows))
//     .catch((error: Error) => response.status(500).send(error));
// });

router.get('/home/user/:username/:token', (request, response) => {
  const username = request.params.username;
  const token = request.params.token;
  passwordService
    .getCredentials(username, token)
    .then((credentials) =>
      credentials ? response.send(credentials) : response.status(404).send('Credentials not found')
    )
    .catch((error) => response.status(500).send(error));
});

// Example request body: { title: "Ny oppgave" }
// Example response body: { id: 4 }
router.post('/home', (request, response) => {
  // console.log('I task-router: ' + response.body.password);
  if (1 == 1)
    passwordService
      .passwordChecker(request.body.username, request.body.password)
      .then((result) => response.send(result))
      .catch((error) => response.status(540).send(error));
  else response.status(550).send('Wrong input');
});

// router.delete('/tasks/:id', (request, response) => {
//   taskService
//     .delete(Number(request.params.id))
//     .then((result) => response.send())
//     .catch((error) => response.status(500).send(error));
// });

export default router;
