// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component, sharedComponentData } from 'react-simplified';
import { Card, Row, Column, Form, Button, Alert } from './widgets';
import passwordService from './task-service';
import { NavLink, HashRouter, Route } from 'react-router-dom';
import { createHashHistory } from 'history';
import sha256 from 'crypto-js/sha256';
import hmacSHA512 from 'crypto-js/hmac-sha512';
import Base64 from 'crypto-js/enc-base64';
import CryptoJS from 'crypto-js';

//const message, nonce, path, privateKey; // ...
//const hashDigest = sha256(nonce + message);
//const hmacDigest = Base64.stringify(hmacSHA512(path + hashDigest, privateKey));

const history = createHashHistory();
class LoginDetails {
  username = '';
  password = '';
  token = '';
  balance = null;
  age = null;
  fullName = '';
  address = '';
  getUsername() {
    return this.username;
  }
  getPassword() {
    return this.password;
  }
  getToken() {
    return this.token;
  }
  setUsername(username) {
    this.username = username;
  }
  setPassword(password) {
    this.password = password;
  }
  setToken(token) {
    this.token = token;
  }
  getBalance() {
    return this.balance;
  }
  getAge() {
    return this.age;
  }
  getFullName() {
    return this.fullName;
  }
  getAddress() {
    return this.address;
  }
  setInitialUserInfo(balance, fullName, address, age) {
    console.log(balance);
    this.balance = balance;
    this.fullName = fullName;
    this.address = address;
    this.age = age;
  }
}
export const loginDetails = sharedComponentData(new LoginDetails());

// import sha256 from 'crypto-js/sha256';
// import hmacSHA512 from 'crypto-js/hmac-sha512';
// import Base64 from 'crypto-js/enc-base64';
// var AES = require("crypto-js/aes");
// var SHA256 = require("crypto-js/sha256");

// console.log(SHA256("Message"));
// var CryptoJS = require("crypto-js");
// console.log(CryptoJS.HmacSHA1("Message", "Key"));

class TaskNew extends Component {
  username_input = '';
  password_input = '';
  salt = 'Very good salt';
  hashFinish = false;
  hashedPassClient = '';

  render() {
    return (
      <Card title="Login">
        <Row>
          <Column width={2}>
            <Form.Label>Username:</Form.Label>
          </Column>
          <Column width={5}>
            <Form.Input
              type="text"
              value={this.username_input}
              onChange={(event) => (this.username_input = event.currentTarget.value)}
            ></Form.Input>
          </Column>
        </Row>
        <Row>
          <Column width={2}>
            <Form.Label>Password:</Form.Label>
          </Column>
          <Column width={5}>
            <Form.Input
              type="password"
              value={this.password_input}
              onChange={(event) => (this.password_input = event.currentTarget.value)}
            ></Form.Input>
          </Column>
        </Row>
        <Button.Success
          onClick={() => {
            this.hashPass();
            passwordService
              .authenticate(this.username_input, this.hashedPassClient.toString())
              .then((result) => {
                console.log(result);
                if (result.login == true) {
                  loginDetails.setUsername(this.username_input);
                  loginDetails.setPassword(this.hashedPassClient);
                  loginDetails.setToken(result.token);
                  history.push('/loggedIn');
                } else {
                  Alert.danger('Wrong username or password');
                }
                // Reloads the tasks in the Tasks component
                //TaskList.instance()?.mounted(); // .? meaning: call Tasks.instance().mounted() if Tasks.instance() does not return null
                // this.title = '';
              });
          }}
        >
          Create
        </Button.Success>
      </Card>
    );
  }
  hashPass() {
    this.hashedPassClient = CryptoJS.PBKDF2(this.password_input, this.salt, { keySize: 512 / 32 });
    console.log(this.hashedPassClient.toString());
  }
}

class LoggedIn extends Component {
  render() {
    return (
      <Card title={'Hello! You are now logged in as : ' + loginDetails.getUsername()}>
        <div>
          <Button.Success
            onClick={() => {
              passwordService
                .getCredentials(loginDetails.getUsername(), loginDetails.getToken())
                .then((result) => {
                  console.log(result);
                  loginDetails.setInitialUserInfo(
                    result.balance,
                    result.fullName,
                    result.address,
                    result.age
                  );
                });
            }}
          >
            Get credentials!
          </Button.Success>
          <Card>
            <Row>
              <Column>Full name: {loginDetails.getFullName()}</Column>
            </Row>
            <Row>
              <Column>Age: {loginDetails.getAge()}</Column>
            </Row>
            <Row>
              <Column>Address: {loginDetails.getAddress()}</Column>
            </Row>
            <Row>
              <Column>Balance: {loginDetails.getBalance()}</Column>
            </Row>
          </Card>
        </div>
      </Card>
    );
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <>
      <HashRouter>
        <div>
          <Alert />
          <Route exact path="/" component={TaskNew} />
          <Route exact path="/loggedIn" component={LoggedIn} />
          {/* <Route path="/tiktoktips" component={TikTok} />
        <Route exact path="/ordbokNy" component={OrdbokNy} />
        <Route exact path="/twitter" component={Twitter} />
        <Route exact path="/oddsNy" component={OddsNy} />
        <Route exact path="/logintest" component={LoginTest} />
        <Route exact path="/leaderboard" component={Leaderboard} />
        <Route exact path="/request" component={RequestBet} />
        <Route exact path="/admin" component={Admin} />
        <Route exact path="/admin/bets/:googleID" component={OtherUser} />
        <Route exact path="/admin/:betID" component={EditBet} /> */}
        </div>
      </HashRouter>
    </>,

    root
  );
