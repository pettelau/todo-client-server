// @flow
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

class PasswordService {
  /**
   * Check if password exists.
   */
  authenticate(username, password) {
    return axios.post('/home', { username, password }).then((response) => response.data);
  }

  /**
   * Get all initial user credentials.
   */
  getCredentials(username, token) {
    console.log(username, token);
    return axios.get('/home/user/' + username + '/' + token).then((response) => response.data);
  }

  // /**
  //  * Create new task having the given title.
  //  *
  //  * Resolves the newly created task id.
  //  */
  // create(title: string) {
  //   return axios
  //     .post<{ title: string }, { id: number }>('/tasks', { title: title })
  //     .then((response) => response.data.id);
  // }
}

const passwordService = new PasswordService();
export default passwordService;
